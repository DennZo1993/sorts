import argparse
import os
import shutil
import subprocess
import traceback

from gen import Generator

DIR = os.path.dirname(os.path.abspath(__file__))
MAIN_CPP = os.path.join(DIR, 'cpp/main.cpp')
CPP_EXEC = os.path.join(DIR, 'push.out')
MAIN_PY = os.path.join(DIR, 'py/main.py')
DATA_DIR = os.path.join(DIR, 'data')
OUTPUT_DIR = os.path.join(DIR, 'output')


class TestFailure(RuntimeError):
    pass


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('-c', dest='compiler', default='g++', help='C++ compilier (default: g++)')
    parser.add_argument('-s', dest='standard', type=int, default=17, help='C++ standard (default: 17)')
    parser.add_argument('-n', dest='nvalues', type=int, default=100, help='Number of values in each test')
    return parser.parse_args()


def build_cpp(args, file):
    cmd = [
        args.compiler,
        '-std=c++{}'.format(args.standard),
        MAIN_CPP,
        file,
        '-o',
        CPP_EXEC
    ]
    try:
        print('Building C++ source: {}'.format(' '.join(cmd)))
        subprocess.check_output(cmd, stderr=subprocess.STDOUT)
        return [CPP_EXEC]
    except subprocess.CalledProcessError as e:
        print('Failed to compile {}\n{}'.format(file, e.output))
        raise


def build_py(file):
    cmd = ['python', MAIN_PY, file]
    print('Building Python source: {}'.format(' '.join(cmd)))
    return cmd


def collect_sources():
    DIRS = ['cpp', 'py']
    NAME_FILTER = ['main.cpp', 'main.py', '__init__.py']

    result = []
    for d in DIRS:
        result += [os.path.join(d, f) for f in os.listdir(d) if f not in NAME_FILTER]
    return result


def build_source(args, source_file):
    if source_file.endswith('.cpp'):
        return build_cpp(args, source_file)
    elif source_file.endswith('.py'):
        return build_py(source_file)


def run_tests(exec_cmd):
    print('===== RUNNING TEST CASES =====')
    clear_artifacts()

    failed_tests = 0
    total_tests = 0
    for input_file in os.listdir(DATA_DIR):
        print('')
        total_tests += 1
        try:
            full_input = os.path.join(DATA_DIR, input_file)
            full_output = os.path.join(OUTPUT_DIR, input_file)

            # Run executable and produce output
            print('Test: {}'.format(full_input))
            cmd = exec_cmd + [full_input, full_output]
            subprocess.check_call(cmd)

            # Read generated output and compare
            check(full_input, full_output)

            print('OK')
        except TestFailure as e:
            failed_tests += 1
            print('Fail: {}'.format(e))
        except Exception as e:
            failed_tests += 1
            print('Err : {}'.format(e))

    if failed_tests == 0:
        msg = 'all tests passed'
    else:
        msg = '{} of {} tests failed'.format(failed_tests, total_tests)
    print('===== {} =====\n'.format(msg))
    return failed_tests




def check(input_file, output_file):
    def read_list(filename):
        result = []
        with open(filename) as f:
            for line in f:
                result += map(int, line.split())
        return result

    input_list = read_list(input_file)
    output_list = read_list(output_file)
    sorted_list = sorted(input_list)
    if output_list != sorted_list:
        len_out = len(output_list)
        len_srt = len(sorted_list)

        reason = 'are not equal'
        if len_out != len_srt:
            reason = 'have different sizes: {} != {}'.format(len_out, len_srt)
        else:
            for i in range(len_out):
                if output_list[i] != sorted_list[i]:
                    reason = 'differ at position {}: {} != {}'.format(i, output_list[i], sorted_list[i])
                    break
        raise TestFailure('Program result and sorted list {}'.format(reason))


def clear_artifacts():
    if os.path.exists(OUTPUT_DIR):
        shutil.rmtree(OUTPUT_DIR)
    os.mkdir(OUTPUT_DIR)


def generate_inputs(args):
    if os.path.exists(DATA_DIR):
        shutil.rmtree(DATA_DIR)
    os.mkdir(DATA_DIR)

    generator = Generator(args.nvalues, -1000000, 100000)
    for name, data in generator.generate():
        filename = '{}.txt'.format(name)
        with open(os.path.join(DATA_DIR, filename), 'w') as f:
            for val in data:
                f.write('{} '.format(val))


def main():
    try:
        args = parse_args()

        generate_inputs(args)

        sources = collect_sources()
        if not sources:
            print('No source files discovered, exiting')
            return 0

        results = {}
        total_failures = 0
        max_filename_len = 0
        for source_file in sources:
            run_cmd = build_source(args, source_file)
            if not run_cmd:
                print('Skipping unrecognized source: {}'.format(source_file))
                continue

            failures = run_tests(run_cmd)
            results[source_file] = failures
            total_failures += failures
            max_filename_len = max(len(source_file), max_filename_len)

        print('\n===== SUMMARY =====')
        for file, failures in results.items():
            s = '{} tests failed'.format(failures) if failures > 0 else 'all tests passed'
            print('{file: <{fill}}: {s}'.format(file=file, s=s, fill=max_filename_len + 1))

        return 0 if total_failures == 0 else 1
    except Exception as e:
        traceback.print_exc()
        return 1


if __name__ == '__main__':
    import sys
    sys.exit(main())
