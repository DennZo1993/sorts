# Sorts
Репозиторий с реализациями алгоритмов сортировки (на C++ и Python) и скриптом для тестирования.

## Запуск тестов

Скрипт `test.py` запускает поиск и проверку всех доступных файлов с реализациями (на C++ и Python).
Можно задать компилятор, стандарт языка C++ и количество чисел в тестовых наборах.<br/>
Подробнее о параметрах скрипта:
```
python test.py -h
```



## Добавление реализации на C++
* Каждая реализация должна быть в отдельном `*.cpp` файле в каталоге `cpp`. 
* В файле с реализацией **внутри неймспейса** `push` должна быть определена функция с сигнатурой `void sort(std::vector<int>& v)`.
* Все, что нужно для реализации функции `sort` должно быть подключено через `#include` (как минимум, `vector`).

**Пример:** *selection sort* (файл `cpp/selection_sort_example.cpp`)
```cpp
#include <vector>

namespace push
{
	void sort(std::vector<int>& v)
	{
	    for (std::size_t i = 0; i + 1 < v.size(); ++i)
	    {
	        auto min = v[i];
	        auto imin = i;
	        for (std::size_t j = i + 1; j < v.size(); ++j)
	        {
	            if (v[j] < min)
	            {
	                min = v[j];
	                imin = j;
	            }
	        }
	        std::swap(v[i], v[imin]);
	    }
	}
} // namespace push
```

## Добавление реализации на Python
* Каждая реализация должна быть в отдельном `*.py` файле в каталоге `py`.
* В файле с реализацией должна быть определена функция с сигнатурой `def push_sort(a)`, модифицирующая переданный массив `a` (т. е. возвращающая `None`).
*  Все, что нужно для реализации функции `push_sort`, должно быть подключено через `#import`.

**Пример:** *selection sort* (файл `py/selection_sort_example.py`)
```py
def push_sort(a):
    for i in range(0, len(a) - 1):
        m = a[i]
        imin = i
        for j in range(i + 1, len(a)):
            if a[j] < m:
                m = a[j]
                imin = j
        a[i], a[imin] = a[imin], a[i]
```