#include <iostream>
#include <fstream>
#include <vector>


namespace push
{
    // Sort function interface. Needs to be linked with object file with implementaion.
    void sort(std::vector<int>&);
}


int main(int argc, char** argv)
{
    if (argc < 3)
    {
        std::cerr << "Not enough arguments" << std::endl;
        return 1;
    }

    std::vector<int> v;

    // Read data
    std::ifstream ifs(argv[1]);
    int currentNumber;
    while (ifs >> currentNumber)
        v.push_back(currentNumber);
    ifs.close();

    // Sort data
    push::sort(v);

    // Write data
    std::ofstream ofs(argv[2]);
    for (auto&& x : v)
        ofs << x << ' ';

    return 0;
}