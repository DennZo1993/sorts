#include <vector>


namespace push
{

void sort(std::vector<int>& v)
{
    for (std::size_t i = 1; i < v.size(); ++i)
    {
        auto tmp = v[i];
        std::size_t j = i;
        while (j > 0 && v[j - 1] > tmp)
        {
            v[j] = v[j - 1];
            --j;
        }
        v[j] = tmp;
    }
}

}