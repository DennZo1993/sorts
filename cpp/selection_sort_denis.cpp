#include <vector>


namespace push
{

void sort(std::vector<int>& v)
{
    for (std::size_t i = 0; i + 1 < v.size(); ++i)
    {
        auto min = v[i];
        auto imin = i;
        for (std::size_t j = i + 1; j < v.size(); ++j)
        {
            if (v[j] < min)
            {
                min = v[j];
                imin = j;
            }
        }
        std::swap(v[i], v[imin]);
    }
}

} // namespace push