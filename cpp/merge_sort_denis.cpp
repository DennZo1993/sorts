#include <vector>

using std::size_t;

namespace
{
    void merge(std::vector<int>& v, size_t l, size_t r, std::vector<int>& tmp)
    {
        size_t q = l + (r - l) / 2;
        size_t tmp_i = 0;
        size_t l_i = l;
        size_t r_i = q + 1;
        while (l_i <= q || r_i <= r)
        {
            int min = 0;
            if (l_i > q)
            {
                min = v[r_i++];
            }
            else if (r_i > r)
            {
                min = v[l_i++];
            }
            else if (v[r_i] < v[l_i])
            {
                min = v[r_i++];
            }
            else
            {
                min = v[l_i++];
            }
            tmp[tmp_i++] = min;
        }
        tmp_i = 0;
        for (size_t i = l; i <= r; ++i, ++tmp_i)
        {
            v[i] = tmp[tmp_i];
        }
    }

    void merge_sort(std::vector<int>& v, size_t l, size_t r, std::vector<int>& tmp)
    {
        if (l == r)
            return;
        size_t q = l + (r - l) / 2;
        merge_sort(v, l, q, tmp);
        merge_sort(v, q + 1, r, tmp);
        merge(v, l, r, tmp);
    }
}


namespace push
{

void sort(std::vector<int>& v)
{
    if (v.empty())
        return;
    std::vector<int> tmp(v.size(), 0);
    merge_sort(v, 0, v.size() - 1, tmp);
}

} // namespace push