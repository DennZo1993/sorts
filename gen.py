import inspect
import random

class Generator(object):
    def __init__(self, count, min, max):
        self.count = count
        self.min = min
        self.max = max

    # All actual generator names MUST start with '_gen'.

    def _gen_same(self):
        data = [random.randint(self.min, self.max)] * self.count
        return ('all_same', data)

    def _gen_random(self):
        data = [random.randint(self.min, self.max) for _ in range(self.count)]
        return ('random', data)

    def _gen_sorted_direct(self):
        _, data = self._gen_random()
        return ('sorted_direct', sorted(data))

    def _gen_sorted_reverse(self):
        _, data = self._gen_random()
        return ('sorted_reverse', sorted(data, reverse=True))

    def _gen_only_negative(self):
        left = -1 * max(abs(self.min), abs(self.max))
        data = [random.randint(left, -1) for _ in range(self.count)]
        return ('only_negative', data)

    def _gen_empty(self):
        return ('empty', [])

    def _gen_single_elem(self):
        return ('single', [random.randint(self.min, self.max)])

    def generate(self):
        # Public interface
        for attr_name in dir(self):
            gen_function = getattr(self, attr_name)
            if inspect.ismethod(gen_function) and attr_name.startswith('_gen'):
                yield gen_function()
