# -*- coding: utf-8 -*-

import traceback
import os
import sys

# command-line args:
# 1. implementation file name (e. g. selection_sort.py)
# 2. input data file (e. g. input.txt)
# 3. output data file (e. g. output.txt)


def filename(s):
    return os.path.splitext(os.path.basename(s))[0]


def run_sort(a):
    # import implementation function
    source_file = filename(sys.argv[1])
    push_sort = __import__(source_file).push_sort
    # run imported function
    push_sort(a)


def main():
    try:
        input_filename = sys.argv[2]
        output_filename = sys.argv[3]

        # read input data
        data = []
        with open(input_filename) as f:
            content = f.read()
            if content:
                data = map(int, content.split())

        # sort
        run_sort(data)

        # save output
        with open(output_filename, 'w') as f:
            for val in data:
                f.write('{} '.format(val))

        return 0
    except:
        # traceback.print_exc()
        return 1


if __name__ == '__main__':
    sys.exit(main())
